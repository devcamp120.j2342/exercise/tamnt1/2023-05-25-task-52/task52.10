
import java.util.ArrayList;

import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrayList = new ArrayList<>();
        Person person0 = new Person();
        Person person1 = new Person("John", "Adam", "70 kg");
        arrayList.add(person1);
        arrayList.add(person0);
        for (Person person : arrayList) {
            System.out.println(person);
        }
    }

}
